# Teste para vaga Front-end na WebJump

![Layout](assets/preview.jpg)

O repositório com a aplicação foi desenvolvido na branch **desafio** e mergeado na branch **master**.

## Tecnologias e ferramentas usadas

- React
- NextJs
- Typescript
- Sass
- Eslint
- Prettier

## Overview das tecnologias escolhidas

Devido ao pouco tempo e as funcionalidades requiridas na aplicação foi optado usar o framework [NextJs](https://nextjs.org/) que é um framework baseado em React e ajuda a tratar funcionalidades como rotas, requests para api, ter suporte a SSR e SSG e etc...

Foi também escolhido usar a ferramenta [Typescript](https://www.typescriptlang.org/) para tipagem, intelisense e segurança no desenvolvimento.

O pré processados [SASS](https://sass-lang.com/) foi escolhido por nos permitir criar novas funcionalidades para o css, como variaveis, funcões que aceitam parametros, nested rules e etc... foi tambem usado o padrão de arquitetura css [BEM](http://getbem.com/) para nomeclatura de classes.


## Como rodar o projeto

Para rodar o projeto clone esse repositório no seu terminal e em seguida rode o comando
```
cd webjump-frontend-teste
```

Logo em seguida instale as dependências do projeto com o comando
```
npm install
```
ou
```
yarn
```

após a instalação das dependências rode o projeto com o comando
```
npm run dev
```
ou
```
yarn dev
```

## Api

Para simular uma verdadeira requisição a uma API, utilizei o próprio sistema de rotas do NextJs que permite simular um servidor em todos os arquivos que estão dentro de ```pages/api```. Segue abaixo os endpoints onde podem ser visto o retorno de cada request:

Pegar lista de itens:
```
http://localhost:3000/api/v1/categories/list
```

Pegar lista de camisetas:
```
http://localhost:3000/api/v1/categories/camisetas
```

Pegar lista de calças:
```
http://localhost:3000/api/v1/categories/calcas
```

Pegar lista de calçados:
```
http://localhost:3000/api/v1/categories/calcados
```

---
made by Washington Campos ❤️.
