import type { NextApiRequest, NextApiResponse } from "next";
import mockData from "../../../../../mock-api/shirts";

export default function handler(_req: NextApiRequest, res: NextApiResponse) {
	res.status(200).json(mockData);
}
