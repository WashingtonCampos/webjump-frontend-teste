import type { NextApiRequest, NextApiResponse } from "next";
import mockData from "../../../../../mock-api/pants";

export default function handler(_req: NextApiRequest, res: NextApiResponse) {
	res.status(200).json(mockData);
}
