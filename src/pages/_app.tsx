import Head from "next/head";
import type { AppProps } from "next/app";

import TopContent from "components/TopContent";
import Header from "components/Header";
import Menu from "components/Menu";
import Footer from "components/Footer";

import "styles/globals.scss";

const MyApp = ({ Component, pageProps }: AppProps) => {
	return (
		<>
			<Head>
				<title>WebJump front-end teste</title>
				<meta
					name="description"
					content="Test for front-end developers made by the company WebJump"
				/>
				<link
					rel="icon"
					href="https://webjump.com.br/wp-content/uploads/2018/08/cropped-favico-32x32.png"
				/>
			</Head>
			<TopContent />
			<Header />
			<Menu />
			<Component {...pageProps} />
			<Footer />
		</>
	);
};

export default MyApp;
