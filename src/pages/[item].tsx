import { GetStaticProps } from "next";

import { queries } from "services/queries";
import { ListItemProps, ProductProps } from "types";

import ItemTemplate from "templates/Item";

export type ItemProps = {
	item: string;
	itemsList: ProductProps[];
};

const Item = ({ item, itemsList }: ItemProps) => {
	return <ItemTemplate item={item} itemsList={itemsList} />;
};

export const getStaticPaths = async () => {
	const { data } = await queries.getCategoriesList();

	const paths = data.items.map(({ path }: ListItemProps) => ({
		params: { item: path },
	}));

	return {
		paths,
		fallback: true,
	};
};

export const getStaticProps: GetStaticProps = async ({ params }) => {
	const { data } = await queries.getItemList(params?.item || "");

	return {
		props: {
			item: params?.item,
			itemsList: data.items,
		},
	};
};

export default Item;
