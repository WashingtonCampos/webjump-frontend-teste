import styles from "components/FilterSidebar/styles.module.scss";

const FilterSidebar = () => {
	return (
		<aside className={styles["filter-sidebar"]}>
			<h1 className={styles["filter-sidebar__title"]}>Filtre por</h1>

			<div className={styles["filter-sidebar__options"]}>
				<h2 className={styles["filter-sidebar__subtitle"]}>Categorias</h2>
				<ul className={styles["filter-sidebar__list"]}>
					<li className={styles["filter-sidebar__item"]}>Roupas</li>
					<li className={styles["filter-sidebar__item"]}>Sapatos</li>
					<li className={styles["filter-sidebar__item"]}>Acessórios</li>
				</ul>
			</div>

			<div className={styles["filter-sidebar__options"]}>
				<h2 className={styles["filter-sidebar__subtitle"]}>Cores</h2>
				<div className={styles["filter-sidebar__colors-list"]}>
					<div className={`${styles["filter-sidebar__color"]} ${styles["filter-sidebar--red"]}`} />
					<div
						className={`${styles["filter-sidebar__color"]} ${styles["filter-sidebar--orange"]}`}
					/>
					<div className={`${styles["filter-sidebar__color"]} ${styles["filter-sidebar--blue"]}`} />
				</div>
			</div>

			<div className={styles["filter-sidebar__options"]}>
				<h2 className={styles["filter-sidebar__subtitle"]}>Tipo</h2>
				<ul className={styles["filter-sidebar__list"]}>
					<li className={styles["filter-sidebar__item"]}>Corrida</li>
					<li className={styles["filter-sidebar__item"]}>Caminhada</li>
					<li className={styles["filter-sidebar__item"]}>Casual</li>
					<li className={styles["filter-sidebar__item"]}>Social</li>
				</ul>
			</div>
		</aside>
	);
};

export default FilterSidebar;
