import { useState, useEffect } from "react";
import { MdList, MdDashboard } from "react-icons/md";

import { ProductProps } from "types";

import { ItemProps } from "pages/[item]";
import ItemCard from "components/ItemCard";

import styles from "components/ItemArea/styles.module.scss";

const ItemArea = ({ item, itemsList }: ItemProps) => {
	const [items, setItems] = useState<ProductProps[]>([]);

	useEffect(() => {
		setItems(itemsList);
	}, [itemsList]);

	const handlePriceOrdenation = (value: string) => {
		if (value === "price") {
			setItems([...items].sort((a, b) => a.price - b.price));
			return;
		}

		setItems([...items].sort((a, b) => a.id - b.id));
	};

	return (
		<section className={styles["item-area"]}>
			<h1 className={styles["item-area__title"]}>{item}</h1>
			<hr />

			<div className={styles["item-area__ordenation"]}>
				<div className={styles["item-area__icons"]}>
					<MdDashboard className={styles["item-area--dashborad"]} />
					<MdList className={styles["item-area--list"]} />
				</div>

				<div className={styles["item-area__order"]}>
					<label className={styles["item-area__label"]}>Ordenar por</label>
					<select
						className={styles["item-area__select"]}
						onChange={e => handlePriceOrdenation(e.target.value)}
					>
						<option value="" />
						<option value="price">Preço</option>
					</select>
				</div>
			</div>
			<hr />

			<section className={styles["item-area__list"]}>
				{items.map(({ id, name, image, price, specialPrice }) => (
					<ItemCard key={id} name={name} image={image} price={price} specialPrice={specialPrice} />
				))}
			</section>
		</section>
	);
};

export default ItemArea;
