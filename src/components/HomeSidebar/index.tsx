import { useState, useEffect } from "react";
import Link from "next/link";

import { queries } from "services/queries";

import styles from "components/HomeSidebar/styles.module.scss";

const HomeSidebar = () => {
	const [categoriesList, setCategoriesList] = useState([]);

	useEffect(() => {
		const getCategories = async () => {
			const { data } = await queries.getCategoriesList();

			setCategoriesList(data.items);
		};

		getCategories();
	}, []);

	return (
		<aside className={styles["home-sidebar"]}>
			<ul className={styles["home-sidebar__menu"]}>
				<li className={styles["home-sidebar__menu-item"]}>
					<Link href="/">
						<a className={styles["home-sidebar__menu-item--link"]}>Página inicial</a>
					</Link>
				</li>

				{categoriesList.map(({ id, name, path }) => (
					<li key={id} className={styles["home-sidebar__menu-item"]}>
						<Link href={`/${path}`}>
							<a className={styles["home-sidebar__menu-item--link"]}>{name}</a>
						</Link>
					</li>
				))}

				<li className={styles["home-sidebar__menu-item"]}>
					<Link href="/">
						<a className={styles["home-sidebar__menu-item--link"]}>Contato</a>
					</Link>
				</li>
			</ul>
		</aside>
	);
};

export default HomeSidebar;
