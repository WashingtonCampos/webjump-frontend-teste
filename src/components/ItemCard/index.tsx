import { formatCurrency } from "utils/formatCurrency";

import styles from "components/ItemCard/styles.module.scss";

type ItemCardProps = {
	name: string;
	image: string;
	price: number;
	specialPrice?: number;
};

const ItemCard = ({ name, image, price, specialPrice }: ItemCardProps) => {
	return (
		<div className={styles["item-card"]}>
			<img className={styles["item-card__img"]} src={image} alt={name} />
			<h3 className={styles["item-card__name"]}>{name}</h3>
			{specialPrice ? (
				<>
					<del className={`${styles["item-card__-price"]} ${styles["item-card__old-price"]}`}>
						{formatCurrency(price)}
					</del>
					<span
						className={`${styles["item-card__special-price"]} ${styles["item-card__current-price"]}`}
					>
						{formatCurrency(specialPrice)}
					</span>
				</>
			) : (
				<span className={`${styles["item-card__price"]} ${styles["item-card__current-price"]}`}>
					{formatCurrency(price)}
				</span>
			)}
		</div>
	);
};

export default ItemCard;
