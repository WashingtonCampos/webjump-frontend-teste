import { useState, useEffect } from "react";
import Link from "next/link";

import { queries } from "services/queries";

import globalElement from "styles/globalElements.module.scss";
import styles from "components/Menu/styles.module.scss";

const Menu = () => {
	const [categoriesList, setCategoriesList] = useState([]);

	useEffect(() => {
		const getCategories = async () => {
			const { data } = await queries.getCategoriesList();

			setCategoriesList(data.items);
		};

		getCategories();
	}, []);

	return (
		<nav className={styles["menu"]}>
			<div className={globalElement["container"]}>
				<div className={styles["menu__wrapper"]}>
					<ul className={styles["menu__list"]}>
						<li className={`${styles["menu__list-item"]} ${styles["menu__list-item--first"]}`}>
							<Link href="/">
								<a className={styles["menu__list-link"]}>Página inicial</a>
							</Link>
						</li>

						{categoriesList.map(({ id, name, path }) => (
							<li key={id} className={styles["menu__list-item"]}>
								<Link href={`/${path}`}>
									<a className={styles["menu__list-link"]}>{name}</a>
								</Link>
							</li>
						))}

						<li className={styles["menu__list-item"]}>
							<Link href="/">
								<a className={styles["menu__list-link"]}>Contato</a>
							</Link>
						</li>
					</ul>
				</div>
			</div>
		</nav>
	);
};

export default Menu;
