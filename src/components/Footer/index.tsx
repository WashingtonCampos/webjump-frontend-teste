import globalElements from "styles/globalElements.module.scss";
import styles from "components/Footer/styles.module.scss";

const Footer = () => {
	return (
		<footer className={styles["footer"]}>
			<div className={globalElements["container"]}>
				<div className={styles["footer__wrapper"]} />
			</div>
		</footer>
	);
};

export default Footer;
