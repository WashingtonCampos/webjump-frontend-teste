import Link from "next/link";

import globalElement from "styles/globalElements.module.scss";
import styles from "components/TopContent/styles.module.scss";

const TopContent = () => {
	return (
		<div className={styles["top"]}>
			<div className={globalElement["container"]}>
				<section className={styles["top__wrapper"]}>
					<Link href="/">
						<a className={styles["top__link"]}>Acesse sua conta</a>
					</Link>
					ou
					<Link href="/">
						<a className={styles["top__link"]}>Cadastre-se</a>
					</Link>
				</section>
			</div>
		</div>
	);
};

export default TopContent;
