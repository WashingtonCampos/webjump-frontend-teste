import Image from "next/image";
import { MdMenu, MdSearch } from "react-icons/md";

import globalElement from "styles/globalElements.module.scss";
import styles from "components/Header/styles.module.scss";

const Header = () => {
	return (
		<header className={styles["header"]}>
			<div className={globalElement["container"]}>
				<div className={styles["header__wrapper"]}>
					<MdMenu className={styles["header__icon"]} size={30} />
					<Image
						src="/logo-webjump.png"
						alt="Texto escrito web jump com um ponto de exclamação"
						width={180}
						height={50}
					/>
					<MdSearch
						className={`${styles["header__icon"]} ${styles["header__icon--search"]}`}
						size={35}
					/>
					<div className={styles["header__search-area"]}>
						<input className={styles["header__input"]} type="text" />
						<button className={styles["header__button"]}>Buscar</button>
					</div>
				</div>
			</div>
		</header>
	);
};

export default Header;
