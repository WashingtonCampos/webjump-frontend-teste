import { api } from "services/api";

export const queries = {
	getCategoriesList: () => api.get("/list"),
	getItemList: (endpoint: string | string[]) => api.get(`/${endpoint}`),
};
