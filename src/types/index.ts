export type ListItemProps = {
	id: number;
	name: string;
	path: string;
};

export type ListProps = {
	items: ListItemProps[];
};

export type FilterProps = {
	color: string;
};

export type ProductProps = {
	id: number;
	sku: string;
	path: string;
	name: string;
	image: string;
	specialPrice?: number;
	price: number;
	filter: FilterProps[];
};
