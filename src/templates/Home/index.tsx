import HomeSidebar from "components/HomeSidebar";
import Content from "components/Content";

import globalElements from "styles/globalElements.module.scss";
import styles from "templates/Home/styles.module.scss";

const HomeTemplate = () => {
	return (
		<main className={styles["home"]}>
			<div className={globalElements["container"]}>
				<div className={styles["home__wrapper"]}>
					<HomeSidebar />
					<Content />
				</div>
			</div>
		</main>
	);
};

export default HomeTemplate;
