import Link from "next/link";

import { ItemProps } from "pages/[item]";

import FilterSidebar from "components/FilterSidebar";
import ItemArea from "components/ItemArea";

import globalElements from "styles/globalElements.module.scss";
import styles from "templates/Item/styles.module.scss";

const ItemTemplate = ({ item, itemsList }: ItemProps) => {
	return (
		<main className={styles["item"]}>
			<div className={globalElements["container"]}>
				<div className={styles["item__wrapper"]}>
					<Link href="/">
						<a className={styles["item__page"]}>Página inicial {">"}</a>
					</Link>

					<Link href={`/${item}`}>
						<a className={`${styles["item__page"]} ${styles["item__page--name"]}`}>{item}</a>
					</Link>

					<section className={styles["item__content"]}>
						<FilterSidebar />
						<ItemArea item={item} itemsList={itemsList} />
					</section>
				</div>
			</div>
		</main>
	);
};

export default ItemTemplate;
